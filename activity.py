
# 1.
year = int(input("Please enter a year: \n"))

if year <= 0:
    print(f"Enter a valid year")
elif year % 4 == 0:
    # Any year that is divisible by 100 should also be divisible by 400 to be consider a leap year (i.e. 1900 is not a leap year)
    if year % 100 == 0 and year % 400 != 0:
        print(f"{year} is not leap year")
    else:
        print(f"{year} is a leap year")
else:
    print(f"{year} is not leap year")


# 2.

col = int(input("Enter number of column/s: \n"))

row = int(input("Enter number of row/s: \n"))

i = 0
while i < row:
    print("*"*col)
    i += 1
    if i == row:
        break
